import React from 'react';
import { store } from './store';
import { Provider } from 'react-redux';
import  Main  from './src/index';

const Root = () => {
  return (
    <Provider store={store}>
      <Main />
    </Provider>
  );
};

export default Root;