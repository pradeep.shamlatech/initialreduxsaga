import React from 'react';
import Navigation from './utils/router';
import { NavigationContainer } from '@react-navigation/native'


const Main = ({ props }) => (
  <NavigationContainer>
    <Navigation />
    </NavigationContainer>
);

export default Main;
