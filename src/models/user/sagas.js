import {call,takeLatest,takeEvery, put} from 'redux-saga/effects';
import {
  REGISTER_USER_INFO_REQUEST,
  REGISTER_USER_INFO_REQUEST_SUCCESS,
  responseApiData

} from './actions';
import {warrentyRegister} from '../api'
function* handler() {
  yield takeLatest(REGISTER_USER_INFO_REQUEST, registerUser);
}

function* registerUser(action) {
  try {
    // API call
    console.log("saga -register USER--89---", action)

    const data = yield call(warrentyRegister)
    console.log("saga -register USER-----",data)

    yield put(responseApiData(data));
  } catch (err) {
    console.log("saga -register USER---ERROR--",err)
    // Handle error
  }
}

export {handler};
