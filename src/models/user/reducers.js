import { REGISTER_USER_INFO_RESPONSE} from './actions';

const initialState = {
  user: {name:'pradeep'},
  token: null,
  data:null
};

const reducer = (state = initialState, obj) => {
  switch (obj.type) {
    case REGISTER_USER_INFO_RESPONSE: {
      console.log('i am in reducer', obj);
      const data = {data:obj.data};
      return {state,...data}
      // return data
    }
    default:
      return state;
  }
};

export {reducer};
