export const REGISTER_USER_INFO_REQUEST = 'REGISTER_USER_INFO_REQUEST';
export const REGISTER_USER_INFO_RESPONSE =
  'REGISTER_USER_INFO_RESPONSE';
export const REGISTER_USER_INFO_REQUEST_FAILURE =
  'REGISTER_USER_INFO_REQUEST_FAILURE';
import { isLoading, error, user } from '../constants/index';

// export const responseApiData = (data) => ({ type: REGISTER_USER_INFO_RESPONSE, data: data }) 
export const registerUser = (data) => ({ type: REGISTER_USER_INFO_REQUEST, data: data })
export const responseApiData = (data) => ({ type: REGISTER_USER_INFO_RESPONSE, data: data }) 
