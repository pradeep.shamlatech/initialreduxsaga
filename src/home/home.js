import React, {useEffect} from 'react';
import { View, Content, Card, Text, Button, Form,Header,Icon,ListItem,CheckBox,Body, Item,Left,Container, Input, Label } from 'native-base'
import {connect} from 'react-redux';
import { registerUser } from '../models/user/actions';

class Home extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      customerName: null,
      customerMobile: null,
      modelPurchased: null,
      pinCode: null,
      state: null,
      dateOfInvoice: null,
      batteryNo: null,
      chasisNo: null,
      modelColor: null,
      financeTroughBajaj: null,
      checkedYes: true,
      checkedNo: false,
      error: []
    };
  }
  componentDidMount() {
  }
  
  
  onSubmit = () => {
    let data = {}
    let errorObj=[]
    this.state.customerName !== null && this.state.customerName.trim().length > 0 ? data.customerName = this.state.customerName : errorObj.push(1)
    this.state.customerMobile !== null && this.state.customerMobile.trim().length > 0 ? data.customerMobile = this.state.customerMobile : errorObj.push(2)
    this.state.modelPurchased !== null && this.state.modelPurchased.trim().length > 0 ? data.modelPurchased = this.state.modelPurchased : errorObj.push(3)
    this.state.pinCode !== null && this.state.pinCode.trim().length > 0 ? data.pinCode = this.state.pinCode : errorObj.push(4)
    this.state.state !== null && this.state.state.trim().length > 0 ? data.state = this.state.state : errorObj.push(5)
    this.state.batteryNo !== null && this.state.batteryNo.trim().length > 0 ? data.batteryNo = this.state.batteryNo : errorObj.push(6)
    this.state.chasisNo !== null && this.state.chasisNo.trim().length > 0 ? data.chasisNo = this.state.chasisNo : errorObj.push(7)
    // this.state.customerName !== null && this.state.customerName.trim().length > 0 ? null : errorObj.push(7)

// will call api if no error found
    if (errorObj.length === 0) {
      this.props.registerUser({data:data})
    } else {
      this.setState({ error: [...errorObj] });
    }
}
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
        </Header>
        <Content>
          <View style={{ marginTop: 30,marginBottom:45,alignSelf:'center' }}>
            <Text style={{ fontSize:20,fontWeight:'bold'}}>WARRENTY REGISTRATION</Text>
            </View>
          <Form>
            <View >
              <View style={{ paddingHorizontal: 25 }}>
                <View>
              <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16, color:'red' }}>Customer Name*</Label>
                <Item regular style={{ backgroundColor: '#f4f4f4', borderTopWidth: 0, borderWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <Input style={{ height: 40, backgroundColor: '#f4f4f4', elevation: 0 }}
                    
                      autoCapitalize='none'
                      autoCorrect={false}
                      // keyboardType='numeric'
                      onChangeText={text => this.setState({ customerName:text})}
                      value={this.state.customerName}
                      
                    />
                </Item>
                  {this.state.error.indexOf(1) !== -1 ?
                  <Label style={{ fontSize: 10, paddingVertical: 4 }} >
                  Please Enter Valid Name !  
                  </Label>
                    : null}
                </View>
                <View>
                  <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16, color: 'red' }}>Customer Mobile*</Label>
                  <Item regular style={{ backgroundColor: '#f4f4f4', borderTopWidth: 0, borderWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <Input style={{ height: 40, backgroundColor: '#f4f4f4', elevation: 0 }}
                      autoCapitalize='none'
                      autoCorrect={false}
                      keyboardType='numeric'
                      onChangeText={text => this.setState({ customerMobile: text })}
                      value={this.state.customerMobile}
                    />
                  </Item>
                  {this.state.error.indexOf(2) !== -1 ?
                    <Label style={{ fontSize: 10, paddingVertical: 4 }} >
                      Please Enter Valid Mobile No. !
                  </Label>
                    : null}
                </View>
                <View>
                  <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16, color: 'red' }}>Model Purchased*</Label>
                  <Item regular style={{ backgroundColor: '#f4f4f4', borderTopWidth: 0, borderWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <Input style={{ height: 40, backgroundColor: '#f4f4f4', elevation: 0 }}
                      autoCapitalize='none'
                      autoCorrect={false}
                      onChangeText={text => this.setState({ modelPurchased: text })}
                      value={this.state.modelPurchased}
                    />
                  </Item>
                  {this.state.error.indexOf(3) !== -1  ?
                    <Label style={{ fontSize: 10, paddingVertical: 4 }} >
                      Please Enter Valid Model Name !
                  </Label>
                    : null}
                </View>
                <View>
                  <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16, color: 'red' }}>Pincode*</Label>
                  <Item regular style={{ backgroundColor: '#f4f4f4', borderTopWidth: 0, borderWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <Input style={{ height: 40, backgroundColor: '#f4f4f4', elevation: 0 }}
                      autoCapitalize='none'
                      autoCorrect={false}
                      onChangeText={text => this.setState({ pinCode: text })}
                      value={this.state.pinCode}
                    />
                  </Item>
                  {this.state.error.indexOf(4) !== -1  ?
                    <Label style={{ fontSize: 10, paddingVertical: 4 }} >
                      Please Enter Valid Pin Code !
                  </Label>
                    : null}
                </View>
                <View>
                  <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16, color: 'red' }}>State*</Label>
                  <Item regular style={{ backgroundColor: '#f4f4f4', borderTopWidth: 0, borderWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <Input style={{ height: 40, backgroundColor: '#f4f4f4', elevation: 0 }}
                      autoCapitalize='none'
                      autoCorrect={false}
                      onChangeText={text => this.setState({ state: text })}
                      value={this.state.state}
                    />
                  </Item>
                  {this.state.error.indexOf(5) !== -1  ?
                    <Label style={{ fontSize: 10, paddingVertical: 4 }} >
                      Please Enter Valid State Name !
                  </Label>
                    : null}
                </View>
                <View>
                  <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16, color: 'red' }}>Date Of Invoice*</Label>
                  <Item regular style={{ backgroundColor: '#f4f4f4', borderTopWidth: 0, borderWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <Input style={{ height: 40, backgroundColor: '#f4f4f4', elevation: 0 }} />
                  </Item>
                  {/* {this.state.error.indexOf(5) !== -1  ?
                    <Label style={{ fontSize: 10, paddingVertical: 4 }} >
                      Please Enter Valid Invoice Date !
                  </Label>
                    : null} */}
                </View>
                <View>
                  <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16, color: 'red' }}>Battery No.*</Label>
                  <Item regular style={{ backgroundColor: '#f4f4f4', borderTopWidth: 0, borderWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <Input style={{ height: 40, backgroundColor: '#f4f4f4', elevation: 0 }}
                      autoCapitalize='none'
                      autoCorrect={false}
                      onChangeText={text => this.setState({ batteryNo: text })}
                      value={this.state.batteryNo}
                    />
                  </Item>
                  {this.state.error.indexOf(6) !== -1  ?
                    <Label style={{ fontSize: 10, paddingVertical: 4 }} >
                      Please Enter Valid Battery Number !
                  </Label>
                    : null}
                </View>
                <View>
                  <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16, color: 'red' }}>Chasis No.*</Label>
                  <Item regular style={{ backgroundColor: '#f4f4f4', borderTopWidth: 0, borderWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <Input style={{ height: 40, backgroundColor: '#f4f4f4', elevation: 0 }}
                      autoCapitalize='none'
                      autoCorrect={false}
                      onChangeText={text => this.setState({ chasisNo: text })}
                      value={this.state.chasisNo}
                    />
                  </Item>
                  {this.state.error.indexOf(7)!== -1 ?
                    <Label style={{ fontSize: 10, paddingVertical: 4 }} >
                      Please Enter Valid Chasis Number !
                  </Label>
                    : null}
                </View>
                <View>
                  <Label style={{ marginBottom: 5, marginTop: 25, fontSize: 16, color: 'red' }}>Model Color*</Label>
                  <Item regular style={{ backgroundColor: '#f4f4f4', borderTopWidth: 0, borderWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, }}>
                    <Input style={{ height: 40, backgroundColor: '#f4f4f4', elevation: 0 }} />
                  </Item>
                
                </View>
             
                <Label style={{ fontSize: 16, color: 'red', marginTop: 25, }} >
                    Finance Through Bajaj
                  </Label>
                <ListItem>
                  <CheckBox onPress={() => this.setState({ checkedYes: !this.state.checkedYes})} iconStyle={{ textAlign: 'center' }} checked={this.state.checkedYes == true ? true : null} style={[{ marginTop: 0, paddingHorizontal: 0, paddingLeft: 0, marginLeft: 0, paddingRight: 0, marginRight: 4,fontSize:20, borderRadius: 2, borderWidth: 2 }, this.state.checkedYes ? { backgroundColor: 'red', borderColor: 'red', } : { backgroundColor: '#f4f4f4', borderColor: '#cccccc',}]} />
                  <Body>
                    <Text>Yes</Text>
                  </Body>
                  <CheckBox onPress={() => this.setState({ checkedNo: !this.state.checkedNo })}  iconStyle={{name:'check', textAlign: 'center' }} Icon={'check'} checked={this.state.checkedNo == true ? true : null} style={[{ marginTop: 0, paddingHorizontal: 0, paddingLeft: 0, marginLeft: 0, paddingRight: 0, marginRight: 4, fontSize: 20, borderRadius: 2, borderWidth: 2 }, this.state.checkedNo ? { backgroundColor: 'red', borderColor: 'red', } : { backgroundColor: '#f4f4f4', borderColor: '#cccccc', }]} />
                  <Body>
                    <Text>No</Text>
                  </Body>
                </ListItem>
              </View> 
              <View style={{alignSelf:'center',marginVertical:20}}>
                <Button style={{ backgroundColor: 'red' }} onPress={() => {
                  this.onSubmit()
                }} success><Text> Success </Text></Button>
              </View>  
            </View>
          </Form>
        </Content>
      </Container>
    );
  }
}


  const mapStateToProps = ({ user }) => {
    return { user: user.data };
  };

  export default connect(
    mapStateToProps,
    { registerUser },
  )(Home);

