import React from 'react'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import Home from '../home/home';



const defaultHeader = () => {
  return {
    headerStyle: {
      backgroundColor: 'white',
    },
    headerTintColor: 'red',
    headerTitleStyle: {
      textAlign: 'center',
      flex: 1,
    },
    headerLayoutPreset: 'center',
    gestureEnabled: true,
    gestureDirection: 'horizontal',
    ...TransitionPresets.SlideFromRightIOS,
    headerMode: 'float',
    animation: 'fade',
  }
}
const MainStack = createStackNavigator()
const AppDrawerNavigation = () => {
  return (
    <MainStack.Navigator
      screenOptions={defaultHeader}>
      <MainStack.Screen name="Home" component={Home} options={{ headerShown: false }} />
    </MainStack.Navigator>
  )

 }


export default AppDrawerNavigation;